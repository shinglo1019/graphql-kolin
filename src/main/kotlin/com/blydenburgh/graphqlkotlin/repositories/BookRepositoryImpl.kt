package com.blydenburgh.graphqlkotlin.repositories

import com.blydenburgh.graphqlkotlin.entities.Book
import com.blydenburgh.graphqlkotlin.entities.User
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Component
import org.springframework.data.mongodb.core.query.Query

@Component
class BookRepositoryImpl(private val mongoOperations: MongoOperations) {
    @Autowired
    lateinit var bookRepository: BookRepository


    fun getAllBook(offset: Long, count: Int, filter: String, sortOrder: String, sortBy: String): List<Book> {
        val query = Query()
        val sorting = if (sortOrder.toLowerCase() === "asc") {
            Sort.Direction.ASC
        } else {
            Sort.Direction.DESC
        }
        query.addCriteria(
                Criteria().orOperator(
                        Criteria.where("name").regex(filter),
                        Criteria.where("author.name").regex(filter)))
                .with(Sort(sorting, sortBy)).skip(offset).limit(count)

        return mongoOperations.find(query, Book::class.java)
    }

    fun getBookById(id: String): List<Book> {
        val query = Query()
        query.addCriteria(Criteria.where("_id").`is`(id))
        return mongoOperations.find(query, Book::class.java)
//        val lookup = LookupOperation.newLookup().from("users").localField("author").foreignField("_id").`as`("users")
//        val match = Aggregation.match(Criteria.where("_id").`is`(id))
////        var list = ArrayList<DBObject>()
////        list.add(lookup.toDBObject(Aggregation.DEFAULT_CONTEXT))
//        val aggregation = Aggregation.newAggregation(Book::class.java, match, lookup)
//
//        val result = mongoOperations.aggregate(aggregation, Book::class.java).mappedResults
//        return result
    }

    fun createBook(name: String, publishDate: String, userId: String): Book {
        val _id = ObjectId()
        val query = Query()
        query.addCriteria(Criteria.where("_id").`is`(userId))
        val user: User = mongoOperations.find(query, User::class.java)[0]
        val book = Book(_id = _id, name = name, publishDate = publishDate, author = user)
        bookRepository.save(book)
        return book
    }
}