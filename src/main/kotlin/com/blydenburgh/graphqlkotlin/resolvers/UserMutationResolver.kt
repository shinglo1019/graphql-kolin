package com.blydenburgh.graphqlkotlin.resolvers

import com.blydenburgh.graphqlkotlin.entities.User
import com.blydenburgh.graphqlkotlin.repositories.UserRepositoryImpl
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserMutationResolver(private val userRepositoryImpl: UserRepositoryImpl) : GraphQLMutationResolver {
    fun createUser(name: String, avatar: String): User {
        return userRepositoryImpl.createUser(name = name, avatar = avatar)
    }

}