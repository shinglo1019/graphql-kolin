package com.blydenburgh.graphqlkotlin.resolvers

import com.blydenburgh.graphqlkotlin.entities.User
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.blydenburgh.graphqlkotlin.repositories.BookRepositoryImpl
import com.blydenburgh.graphqlkotlin.entities.Book
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class BookQueryResolver(private val bookRepositoryImpl: BookRepositoryImpl) : GraphQLQueryResolver {
    fun getBookById(id: String): List<Book> {
        return bookRepositoryImpl.getBookById(id)
    }

    fun getAllBook(offset: Long, count: Int, filter: String, sortOrder: String, sortBy: String): List<Book> {
        return bookRepositoryImpl.getAllBook(offset, count, filter, sortOrder, sortBy)
    }
//    fun getUser(bookId: String): List<User> {
//        val query = Query()
//        query.addCriteria(Criteria.where("bookId").`is`(bookId))
//        return mongoOperations.find(query, User::class.java)
//    }
//
//    fun getBook(): String {
//        return "Book"
//    }
}