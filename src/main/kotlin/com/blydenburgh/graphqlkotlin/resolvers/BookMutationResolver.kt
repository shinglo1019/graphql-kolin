package com.blydenburgh.graphqlkotlin.resolvers

import com.blydenburgh.graphqlkotlin.entities.Book
import com.blydenburgh.graphqlkotlin.entities.User
import com.blydenburgh.graphqlkotlin.repositories.BookRepositoryImpl
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.bson.types.ObjectId
import org.springframework.stereotype.Component
import java.util.*

@Component
class BookMutationResolver (val bookRepositoryImpl: BookRepositoryImpl): GraphQLMutationResolver {
    fun createNewBook(name: String, publishDate: String, userId: String): Book {
        return bookRepositoryImpl.createBook(name = name, publishDate = publishDate, userId = userId )
    }

}