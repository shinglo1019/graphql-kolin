package com.blydenburgh.graphqlkotlin.entities

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "books")
data class Book(
        val _id: ObjectId,
        val name: String,
        val publishDate: String,
        var author: User
//        val comment: ArrayList<Comment>? = null
)