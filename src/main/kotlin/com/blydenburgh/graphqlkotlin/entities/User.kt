package com.blydenburgh.graphqlkotlin.entities

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "users")
data class User(
        val _id: ObjectId,
        val name: String,
        val avatar: String
)